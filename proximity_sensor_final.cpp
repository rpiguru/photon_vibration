#include "Ubidots/Ubidots.h"


// Variables to be changed...
#define TOKEN "BG73aNkVcM2Ueb8r5DSjK4MtJQWoXe"      // Put here your Ubidots TOKEN
#define DATA_SOURCE_NAME "motion"                   // Your Data Source Name
#define VARIABLE_NAME "ttt"                         // Variable name of the data source above.       


/////     Change this value if you need to upload data in different time interval.
int upload_interval = 10;       // Uploading interval in seconds.



int ledPin = D7;                // Built-in LED to indicate motion is detected or not.
int motionPin = D0;             // Motion Sensor pin.
Ubidots ubidots(TOKEN);

void setup() {
  pinMode(ledPin, OUTPUT); 
  pinMode(motionPin, INPUT); 
  digitalWrite(ledPin, LOW);
  
}


void loop() {
  if (!digitalRead(motionPin)){
	  ubidots.add(VARIABLE_NAME, 1);  
	  digitalWrite(ledPin, HIGH);
  }
  else{
	  ubidots.add(VARIABLE_NAME, 0);  
	  digitalWrite(ledPin, LOW);
  }
  ubidots.sendAll();

  delay(1000 * upload_interval);
  
}
