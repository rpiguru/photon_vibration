
//////    Pin Assignment    //////
int ledPin = D7;                // Built-in LED to indicate motion is detected or not.
int motionPin = D0;             // Motion Sensor pin.
int proximityPin = A0;          // Proximity Sensor pin.

/////     Change this value if you need to upload data in different time interval.
int upload_interval = 10;       // Uploading interval in minutes.

/// global variables for motion counting
int counter = 0;                // counter variable
int motion_value = 0;           // store motion value


void setup() {
  pinMode(ledPin, OUTPUT);
  pinMode(motionPin, INPUT);
  digitalWrite(ledPin, LOW);
  counter = 0;
  motion_value = 0;

}


void loop() {

  if (counter < 60 * upload_interval){
    counter ++;
	// If proximity sensor detects, increase counter value and turn LED on
    if (!digitalRead(motionPin)){
      motion_value ++;
      digitalWrite(ledPin, HIGH);
    }
	// Otherwise turn LED off
    else{
      digitalWrite(ledPin, LOW);
    }
  }
  else{
    // Publish motion value and initialize them, the event name is "motion"
    Particle.publish("motion", String(motion_value), PRIVATE);
    counter = 0;
    motion_value = 0;
  }
  
  // We will check status of motion sensor every second.
  delay(1000);
}
